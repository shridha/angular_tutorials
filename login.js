var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "login.html"
    })
    .when("/dashboard", {
        resolve:{
          "check": function($location,$rootScope){
            if(!$rootScope.isLoggedIn) {
              $location.path('/');  
          }
        }
      },
        templateUrl : "dashboard.html"
    });
});



app.controller('loginctrl',function($scope,$location,$rootScope){

    $scope.submit = function(){
        var uname = $scope.username;
        var password =$scope.password;

        if(uname=="admin" && password=="admin123"){
            $rootScope.isLoggedIn = true;
            $location.path('/dashboard');
        }
        else
            alert("Wrong credentials!Please try again");
  };
});